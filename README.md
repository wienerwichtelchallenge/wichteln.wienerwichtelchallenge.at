# wichteln.wienerwichtelchallenge.at

*In der lebenswertesten Stadt der Welt haben heuer ALLE einen Grund zum Feiern. Gemeinsam erfüllen wir ALLE Wünsche!*

## Setup

- make sure [node.js](http://nodejs.org) is at version >= `6`
- clone this repo and `cd` into the folder
- run `yarn`

## Development

- run `yarn run dev`

## Build Setup

Build for production with minification

- `yarn run build`

Build for production and view the bundle analyzer report

- `yarn run build --report`

## Testing

Tests are located in `test/**` and are powered by [ava](https://github.com/sindresorhus/ava)

- `yarn` to ensure devDeps are installed
- `yarn run unit` to run unit tests
- `yarn run test` to run e2e tests
- `yarn run test` to run all tests

## Deployment

Deployment is performed with as simple bash script

- `chmod +x deploy.sh`
- `./deploy.sh`

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
