#!/usr/bin/env bash
yarn run build
rsync -vzcrSLh ./dist/ web@wienerwichtelchallenge.at:/srv/www/wichteln.wienerwichtelchallenge.at/web

# The options:
# v - verbose
# z - compress data
# c - checksum, use checksum to find file differences
# r - recursive
# S - handle sparse files efficiently
# L - follow links to copy actual files
# h - show numbers in human-readable format
# p - keep local file permissions (not necessarily recommended)
# --exclude - Exclude files from being uploaded
